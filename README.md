#MyJNI

##前言
* 这个工程主要是为练习在android studio（简称AS）中使用ndk编程所使用。
* 参考教程网址：[http://demo.netfoucs.com/ashqal/article/details/21869151](http://demo.netfoucs.com/ashqal/article/details/21869151)
* 参考教程中使用的Android Studio是0.4.2
* 本工程的创建是在android studio 1.2 preview 4 版本中完成。
* 推荐在AS中使用插件“jni helper”，可以方便快速的生成.h头文件
* * *

##工程简介
工程分为两个module

1. **app：** 创建工程时，自动生成的module
2. **hellojni：** 手动创建android library module：hellojni，有关jni方面的代码全部放到了hellojni这个module中。

##说明

> 如果没有安装jni helper的话，会比较麻烦，需要手工运行javah来生成.h文件，或者手动在hellojni/autojavah.sh(针对于非windows系统)方法中添加对应的代码来完成，总之么有jni helper是比较麻烦的。
> 
> 如果有了jni helper 可以直接在需要生成.h文件的java编辑器中右键单击空白区域，就会看到创建.h文件的菜单选项。

###jni在android studio中的配置如下
1.在`MainActivity`中创建native方法，例如：

    public native String getStringFromNative();
2.使用jni helper或者`autojavah.sh`或者手工javah生成`.h`文件。将生成的.h文件移动到`hellojni/src/main/jni`中，然后实现它。

3.（可选）设置hellojni的ndk配置，`hellojni/build.gradle`

    android {
        ...
        buildTypes {
            release {
                ...
                ndk {
                    moduleName "jnimain"
                }
            }
    
            debug {
                ...
                ndk {
                    moduleName "jnimain"
                }
            }
        }
    }
moduleName是静态库的名称，如果不设置，静态库会和module名字一样。

4.引入静态库。在MainActivity中使用静态代码块引入静态库

    static {
            System.loadLibrary("jnimain");
    }
如果没有设置moduleName，则替换jnimain为hellojni。

具体生成的静态库是什么名字，可以在`hellojni/build/intermediates/ndk`的子目录中查看`.mk`文件